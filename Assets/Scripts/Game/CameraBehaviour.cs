﻿using UnityEngine;
using System.Collections;
using System;

public class CameraBehaviour : MonoBehaviour {

    public float Speed=0.5f;
    private bool m_move;
    private Vector3 m_target;
    private Action m_callback;

    void Update()
    {
        if (m_move)
        {
            Vector3 target = new Vector3(m_target.x, m_target.y, transform.position.z);
            transform.localPosition = Vector3.Lerp(transform.position, target, 0.01f * Speed);

            if (Vector3.Distance(target, transform.localPosition) < 0.1f)
            {
                m_move = false;
                m_callback.Invoke();
            }
        }
    }

    public void MoveTo(Vector3 pos, Action callback)
    {
        m_callback = callback;
        m_move = true;
        m_target = pos;
    }

}
