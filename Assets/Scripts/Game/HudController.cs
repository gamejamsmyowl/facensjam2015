﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.Game
{
    public class HudController : MonoBehaviour
    {
        public UILabel deathsLabel;
        public UILabel keysLabel;
        public UILabel memoriesLabel;
        public UILabel timeLabel;

        public int DeathsCount { get; set; }
        public int KeysCount { get; set; }
        public int MemoriesCount { get; set; }

        private int _countTimerMinutes = 0;
        private int _countTimerSeconds = 0;
        private bool _isPaused = false;

        private GameController m_gameController;

        void Start()
        {
            m_gameController = FindObjectOfType<GameController>();
            StartCoroutine(TimerOfMap());
        }

        public void RefreshHud()
        {
            deathsLabel.text = string.Format("x{0}", DeathsCount.ToString());
            keysLabel.text = string.Format("x{0}", KeysCount.ToString());
            memoriesLabel.text = string.Format("x{0}", MemoriesCount.ToString());
        }

        public void Pause()
        {
            _isPaused = !_isPaused;
            //m_gameController.Pause();
            //panelMenuPause.enable = true;
        }

        public void Resume()
        {
            _isPaused = !_isPaused;
            //panelMenuPause.enable = false;
            //m_gameController.Resume();
        }

        private IEnumerator TimerOfMap()
        {
            yield return new WaitForSeconds(1);
            if (!_isPaused)
            {
                _countTimerSeconds++;
                if (_countTimerSeconds > 59)
                {
                    _countTimerMinutes++;
                    _countTimerSeconds = 0;
                }
                timeLabel.text = string.Format("{0}:{1}", (_countTimerMinutes).ToString("00"), (_countTimerSeconds).ToString("00"));
            }
            StartCoroutine(TimerOfMap());
        }
    }
}
