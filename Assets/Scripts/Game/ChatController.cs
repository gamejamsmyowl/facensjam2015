﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.Game
{
    public class ChatController : MonoBehaviour
    {
        public UILabel labelMessage;
        public GameObject ModalChat;
        private GameController m_gameController;
        private Action m_callback;

        void Start()
        {
            m_gameController = FindObjectOfType<GameController>();
        }

        public void ShowChat(string message,Action callack)
        {
            m_callback = callack;
            labelMessage.text = message;
            if (!ModalChat.activeSelf)
            {
                ModalChat.SetActive(true);
            }
        }

        void FixedUpdate()
        {
            if (Input.GetMouseButtonDown(0) && GameObject.FindObjectOfType<GameController>().m_gameState == GameController.GameState.Chat)
            {
                ModalChat.SetActive(false);
                if(m_callback != null)
                    m_callback.Invoke();
            }
        }
    }
}
