﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.Game
{
    public class MemoryBehaviour : MonoBehaviour
    {
        public Animator animator;
        public string Memory;
        public bool DiscoveredMemory;

        Action m_callback;
        /// <summary>
        /// Animation of monster appearing
        /// </summary>
        public void ShowAnimation(Action CallBack)
        {
            animator.SetTrigger(Memory);

            m_callback = CallBack;
        }

        public void OnFinishAnimation()
        {
            if(m_callback!=null)
              m_callback.Invoke();
        }
    }
}
