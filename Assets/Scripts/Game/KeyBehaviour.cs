﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.Game
{
    public class KeyBehaviour : MonoBehaviour
    {
        /// <summary>
        /// Animation of monster appearing
        /// </summary>
        public void ShowAnimation(Action CallBack)
        {
            CallBack.Invoke();
        }
    }
}
