﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TypingEffect : MonoBehaviour
{
    public Text textComponent;
    public float timeDelay;

    private string m_currentText;

    private int m_counter;

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
            SetText("BANANA");
    }

    public void SetText(string text)
    {
        m_currentText = text;

        textComponent.text = string.Empty;
        m_counter = 0;

        StartCoroutine(SetTextRoutine());
    }

    public IEnumerator SetTextRoutine()
    {
        while (m_counter < m_currentText.Length)
        {
            yield return new WaitForSeconds(timeDelay);

            textComponent.text += m_currentText[m_counter];

            m_counter++;
        }
    }
}
